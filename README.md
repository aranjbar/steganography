## STEGANOGRAPHY
Hiding text within an image file. A program written in C++ to hide text with a simple segmentation algorithm for bitmap images. 

Advanced Programming, University of Tehran