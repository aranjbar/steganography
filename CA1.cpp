#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <stdlib.h>
using namespace std;

#define red 0
#define green 1
#define blue 2

typedef struct pixel
{
	unsigned int rgb_blue;
	unsigned int rgb_green;
	unsigned int rgb_red;
}pixel;

typedef struct bitmap_info_header
{
	int biWidth;
	int biHeight;
}bitmap_info_header;

typedef struct bitmap_file_header
{
	unsigned short bfType;
	unsigned int   bfSize;
	unsigned short bfReserved1;
	unsigned short bfReserved2;
	unsigned int   bfOffBits;
}bitmap_file_header;

typedef struct square
{
	unsigned int x;
	unsigned int y;
	double variance;
	unsigned int color;
}square;

vector<vector<pixel> > img_read(string file_name ,int (&bitmap_header)[54])
{
	int error = 0, width, height, zeros = 0, type;
	do
	{
		if(error == -1)
		{
			cout << "Enter file name again: " << endl;
			cin >> file_name;
		}
		ifstream img_file(file_name.c_str());
		if(!img_file)
		{
			cerr << "can't open file [" << file_name << "]" << endl;
			exit (EXIT_FAILURE);
		}
		for(int i = 0; i < 54; i++)
			bitmap_header[i] = (unsigned int)img_file.get();
		if((type = bitmap_header[0]  + 256*bitmap_header[1]) != 19778)
		{
			cout << "error! [" << file_name << "]" << "is not a bitmap file.";
			error = -1;
		}
		if(error != -1)
		{
			width  = bitmap_header[18] + 256*bitmap_header[19] + 256*256*bitmap_header[20] + 256*256*256*bitmap_header[21];
			height = bitmap_header[22] + 256*bitmap_header[23] + 256*256*bitmap_header[24] + 256*256*256*bitmap_header[25];
			vector<vector<pixel> > bitmap_pixel_array(height);
			if(((3*width)%4) != 0)
				zeros = 4-((3*width)%4);
			for(int j = 0; j < height; j++)
			{
				vector<int> current_row; 
				for(int i = 0; i < (3*width+zeros); i++)
					current_row.push_back((unsigned int)img_file.get());
				for(int i = 0; i < 3*width; i+=3)
				{
					pixel one_pixel;
					one_pixel.rgb_blue  = current_row[i];
					one_pixel.rgb_green = current_row[i+1];
					one_pixel.rgb_red   = current_row[i+2];
					bitmap_pixel_array[j].push_back(one_pixel);
				}
			}
			return bitmap_pixel_array;
		}
	}while(error == -1);
}

void img_properties(int (&bitmap_header)[54])
{
	bitmap_file_header info1;
	bitmap_info_header info2;
	info1.bfType      = bitmap_header[0]  + 256*bitmap_header[1];
	info1.bfSize      = bitmap_header[2]  + 256*bitmap_header[3]  + 256*256*bitmap_header[4]  + 256*256*256*bitmap_header[5];
	info1.bfReserved1 = bitmap_header[6]  + 256*bitmap_header[7];
	info1.bfReserved2 = bitmap_header[8]  + 256*bitmap_header[9];
	info1.bfOffBits   = bitmap_header[10] + 256*bitmap_header[11] + 256*256*bitmap_header[12] + 256*256*256*bitmap_header[13];
	info2.biWidth     = bitmap_header[18] + 256*bitmap_header[19] + 256*256*bitmap_header[20] + 256*256*256*bitmap_header[21];
	info2.biHeight    = bitmap_header[22] + 256*bitmap_header[23] + 256*256*bitmap_header[24] + 256*256*256*bitmap_header[25];
	if(info1.bfType == 19778)
		cout << "\e[1mImage Type  \e[0mbmp(BMP)" << endl;
	cout << "\e[1mSize  \e[0m      " << info1.bfSize/1000.0 << " kB" << endl;
	cout << "\e[1mWidth  \e[0m     " << info2.biWidth << " pixels" << endl;
	cout << "\e[1mHeight  \e[0m    " << info2.biHeight << " pixels" << endl;
}

square calc_square(vector<pixel> square_pixels, int x, int y, int color)
{
	square result;
	// Calculate sum
	double sum = 0;
	double variance = 0;
	for(int i = 0; i < 64; i++)
	{
		if(color == red)        sum += (square_pixels[i].rgb_red   & 254);
		else if(color == green) sum += (square_pixels[i].rgb_green & 254);
		else if(color == blue)  sum += (square_pixels[i].rgb_blue  & 254); 
	} 
	// Calculate var
	for(int i = 0; i < 64; i++)
	{
		if(color == red)
			variance =+ (sum/64 - (square_pixels[i].rgb_red & 254))*(sum/64 - (square_pixels[i].rgb_red & 254));
		else if(color == green)
			variance =+ (sum/64 - (square_pixels[i].rgb_green & 254))*(sum/64 - (square_pixels[i].rgb_green & 254));
		else if(color == blue)
			variance =+ (sum/64 - (square_pixels[i].rgb_blue & 254))*(sum/64 - (square_pixels[i].rgb_blue & 254));	
	}
	
	result.x = x;
	result.y = y;
	result.variance = -1 * variance;
	result.color = color;
	return result;
}

void swap_square(square& a, square& b)
{
	square temp;

	temp.x        = a.x;
	temp.y        = a.y;
	temp.variance = a.variance;
	temp.color    = a.color;

	a.x        = b.x;
	a.y        = b.y;
	a.variance = b.variance;
	a.color    = b.color;

	b.x        = temp.x;
	b.y        = temp.y;
	b.variance = temp.variance;
	b.color    = temp.color;
}

int partition(vector<square>& array , int left , int right)
{
	int middle;

	double x = array[left].variance;
	int l = left;
	int r = right;
	while(l < r){
		while((array[l].variance <= x) && (l < right)) l++;
		while((array[r].variance > x) && (r >= left)) r--;
		if(l < r)
			swap_square(array[l], array[r]);
	}
	middle = r ;
	swap_square(array[left], array[middle]);
	return middle ;
}

void quicksort(vector<square>& array , int left , int right)
{
	if (left < right){
		int middle = partition(array , left , right) ;
		quicksort(array , left , middle-1) ;
		quicksort(array , middle+1 , right);
	}
}

vector<square> segmentation(vector<vector<pixel> >& bitmap_pixel_array)
{
	int width = bitmap_pixel_array[0].size();
	int height = bitmap_pixel_array.size();
	int w, h;
	vector<square> squares;
	for(int j = 0; j < (height - height % 8); j+=8)
	{
		for(int i = 0; i < (width - width % 8); i+=8)
		{
			w = i; h = j;
			vector<pixel> square_pixels(64);
			for(int k = 0; k < 64; k++)
			{
				square_pixels[k].rgb_red   = bitmap_pixel_array[h][w].rgb_red;
				square_pixels[k].rgb_green = bitmap_pixel_array[h][w].rgb_green;
				square_pixels[k].rgb_blue  = bitmap_pixel_array[h][w].rgb_blue;
				if(w%8 == 7)
				{
					h++;
					w -= 8;
				}
				w++;			
			}
			squares.push_back(calc_square(square_pixels, i, j, blue));
			squares.push_back(calc_square(square_pixels, i, j, green));
			squares.push_back(calc_square(square_pixels, i, j, red));
		}
	}
	quicksort(squares, 0, squares.size() - 1);
	return squares;
}

void img_encrypt(vector<vector<pixel> >& bitmap_pixel_array, vector<square> squares, string message, int seed)
{
	srand(seed);
	int bit_selector = 128;
	message = message + '\n';
	for(int i = 0; i < message.size(); i++)
	{
		bit_selector = 128;
		int letter = (int)message[i];
		for(int j = 0; j < 8; j++)
		{
			int random = rand()%64;
			int w = squares[8*i + j].x + (random % 8);
			int h = squares[8*i + j].y + (random / 8);
			int bit = letter & bit_selector;
			bit /= bit_selector;
			bit_selector /= 2;
			if(bit == 1)
			{
				if(squares[8*i + j].color == red)   bitmap_pixel_array[h][w].rgb_red   = bitmap_pixel_array[h][w].rgb_red   | 1;
				if(squares[8*i + j].color == green) bitmap_pixel_array[h][w].rgb_green = bitmap_pixel_array[h][w].rgb_green | 1;
				if(squares[8*i + j].color == blue)  bitmap_pixel_array[h][w].rgb_blue  = bitmap_pixel_array[h][w].rgb_blue  | 1;
			}
			if(bit == 0)
			{
				if(squares[8*i + j].color == red)   bitmap_pixel_array[h][w].rgb_red   = bitmap_pixel_array[h][w].rgb_red   & 254;			
				if(squares[8*i + j].color == green) bitmap_pixel_array[h][w].rgb_green = bitmap_pixel_array[h][w].rgb_green & 254;
				if(squares[8*i + j].color == blue)  bitmap_pixel_array[h][w].rgb_blue  = bitmap_pixel_array[h][w].rgb_blue  & 254;
			}
		}
	}
}

void img_write(vector<vector<pixel> >& bitmap_pixel_array, int (&bitmap_header)[54], string file_name)
{
	int width  = bitmap_pixel_array[0].size();
	int height = bitmap_pixel_array.size();
	int zeros  = 0;
	unsigned char zero = 0;
	ofstream img_out(file_name.c_str());
	if(!img_out)
	{
		cerr << "can't open file [" << file_name << "]" << endl;
		exit(EXIT_FAILURE);
	}
	for(int i = 0; i < 54; i++)
		img_out << (char)bitmap_header[i];
	if(((3*width)%4) != 0)
		zeros = 4-((3*width)%4);
	for(int j = 0; j < height; j++)
	{
		for(int i = 0; i < width; i++)
		{
			img_out << (unsigned char)bitmap_pixel_array[j][i].rgb_blue << (unsigned char)bitmap_pixel_array[j][i].rgb_green << (unsigned char)bitmap_pixel_array[j][i].rgb_red;
		}
		for(int i = 0; i < zeros; i++)
			img_out << zero;
	}	
}

void img_decrypt(vector<vector<pixel> >& bitmap_pixel_array, vector<square> squares, int seed)
{
	srand(seed);
	int letter = 0;
	int j = 0;
	string message = "";
	do
	{
		letter = 0;
		for(int i = 0; i < 8; i++)
		{
			int random = rand()%64;
			int w = squares[8*j+i].x + (random % 8);
			int h = squares[8*j+i].y + (random / 8);
			int bit;
			if(squares[8*j+i].color == red)   bit = bitmap_pixel_array[h][w].rgb_red%2;
			if(squares[8*j+i].color == green) bit = bitmap_pixel_array[h][w].rgb_green%2;
			if(squares[8*j+i].color == blue)  bit = bitmap_pixel_array[h][w].rgb_blue%2;
			letter = 2 * letter + bit;
		}
		cout << (char)letter;
		j++;
	}while(letter != 10);
}

/*
void img_print(vector<vector<pixel> > bitmap_pixel_array)
{
	int width  = bitmap_pixel_array[0].size();
	int height = bitmap_pixel_array.size();
	for(int j = 0; j < height; j++)
	{
		for(int i = 0; i < width; i++)
		{
			cout << bitmap_pixel_array[j][i].rgb_red << ' ' << bitmap_pixel_array[j][i].rgb_green << ' ' << bitmap_pixel_array[j][i].rgb_blue << "  ";
		}
		cout << endl;
	}
	cout << width << ' ' << height << endl;
}
*/

string cmd_get()
{
	string cmd;
	cin >> cmd;
	return cmd;
}

bool is_valid_cmd(string cmd)
{
	if(cmd == "encrypt" || cmd == "decrypt")
		return true;
	return false;
}

bool is_valid_file(string file_name)
{
	transform(file_name.begin(), file_name.end(), file_name.begin(), ::tolower);
	int f1 = file_name.size() - 4;
	int f2 = file_name.size() - 3;
	int f3 = file_name.size() - 2;
	int f4 = file_name.size() - 1;
	if(file_name[f1] == '.' && file_name[f2] == 'b' && file_name[f3] == 'm' && file_name[f4] == 'p')
		return true;
	return false;
}

bool is_valid_seed(int seed)
{
	if(seed < 1000 || seed > 999999)
		return false;
	return true;
}

bool is_valid_message(string message, int size)
{
	if((message.size() * 8 + 8) >= size)
		return false;
	return true;
}

void cmd_run(string cmd)
{
	while(!is_valid_cmd(cmd))
	{
		cout << "Invalid command. Try again: " << endl;
		cin >> cmd;
	}
	if(cmd == "encrypt")
	{
		//Get file name
		string file_name;
		cout << "Enter file name: " << endl;
		cout << ">> ";
		cin >> file_name;
		while(!is_valid_file(file_name))
		{
			cout << "[" << file_name << "] is not a bmp image. Please enter a valid image name: " << endl;
			cin >> file_name;
		}
	
		//Get seed
		int seed;
		cout << "Enter seed: " << endl;
		cout << ">> ";
		cin >> seed;
		while(!is_valid_seed(seed))
		{
			cout << "[" << seed << "] is out of range. Please enter a valid seed: " << endl;
			cin >> seed;
		}
		
		int bitmap_header[54] = {0};
		vector<vector<pixel> > bitmap_pixel_array;
		vector<square> squares;
		string out_file_name = "";
		for(int i = 0; i < file_name.size() - 4; i++)
		{
			out_file_name += " ";
			out_file_name[i] = file_name[i];
		}
		out_file_name += "-watermarked.bmp";
		bitmap_pixel_array = img_read(file_name ,bitmap_header);
		squares            = segmentation(bitmap_pixel_array);
		
		//Get message
		string message;
		int size = squares.size();
		cout << "Enter message: " << endl;
		cout << ">> ";
		cin.get();
		std::getline(std::cin, message);
		while(!is_valid_message(message, size))
		{
			cout << "This message is too long. I can watermark " << size << " characters at most." << endl;
			std::getline(std::cin, message);
		}
		img_encrypt(bitmap_pixel_array, squares, message, seed);
		img_write(bitmap_pixel_array, bitmap_header, out_file_name);
	}
	else if(cmd == "decrypt")
	{
		//Get file name
		string file_name;
		cout << "Enter file name: " << endl;
		cout << ">> ";
		cin >> file_name;
		while(!is_valid_file(file_name))
		{
			cout << "[" << file_name << "] is not a bmp image. Please enter a valid image name: " << endl;
			cin >> file_name;
		}
			int bitmap_header[54] = {0};
		vector<vector<pixel> > bitmap_pixel_array;
		vector<square> squares;
		bitmap_pixel_array = img_read(file_name ,bitmap_header);
		squares            = segmentation(bitmap_pixel_array);
			//Get seed
		int seed;
		cout << "Enter seed: " << endl;
		cout << ">> ";
		cin >> seed;
		while(!is_valid_seed(seed))
		{
			cout << "[" << seed << "] is out of range. Please enter a valid seed: " << endl;
			cin >> seed;
		}
		img_decrypt(bitmap_pixel_array, squares, seed);
	}
}
int main()
{
	string cmd;
	cmd = cmd_get();
	cmd_run(cmd);
}